import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, Platform } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import * as moment from 'moment';
import { SocialSharing } from '@ionic-native/social-sharing';
import { File } from '@ionic-native/file';
import * as XLSX from 'xlsx';

@IonicPage()
@Component({
  selector: 'page-geofence-report',
  templateUrl: 'geofence-report.html',
})
export class GeofenceReportPage implements OnInit {

  geofenceRepoert: any[] = [];
  Ignitiondevice_id: any;
  datetimeEnd: string;
  datetimeStart: string;
  islogin: any;
  geofencelist: any;
  devicesReport: any;
  geofencedata: any[];
  StartTime: string;
  Startetime: string;
  Startdate: string;
  datetime: number;
  geofenceReportdata: any[] = [];
  locationEndAddress: any;
  twoMonthsLater: any = moment().subtract(2, 'month').format("YYYY-MM-DD");
  today: any = moment().format("YYYY-MM-DD");
  pltStr: string;

  constructor(
    public file: File,
    public socialSharing: SocialSharing,
    public navCtrl: NavController,
    public navParams: NavParams,
    public apicallGeofenceReport: ApiServiceProvider,
    public toastCtrl: ToastController,
    private plt: Platform
  ) {
    if (this.plt.is('android')) {
      this.pltStr = 'md';
    } else if (this.plt.is('ios')) {
      this.pltStr = 'ios';
    }
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    this.datetimeStart = moment({ hours: 0 }).format();
    this.datetimeEnd = moment().format();//new Date(a).toISOString();
  }

  ngOnInit() {
    this.getgeofence1();
  }

  OnExport = function () {
    let sheet = XLSX.utils.json_to_sheet(this.geofenceReportdata);
    let wb = {
      SheetNames: ["export"],
      Sheets: {
        "export": sheet
      }
    };

    let wbout = XLSX.write(wb, {
      bookType: 'xlsx',
      bookSST: false,
      type: 'binary'
    });

    function s2ab(s) {
      let buf = new ArrayBuffer(s.length);
      let view = new Uint8Array(buf);
      for (let i = 0; i != s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
      return buf;
    }

    let blob = new Blob([s2ab(wbout)], { type: 'application/octet-stream' });
    let self = this;

    // this.getStoragePath().then(function (url) {
    self.file.writeFile(self.file.dataDirectory, "geofence_report_download.xlsx", blob, { replace: true })
      .then((stuff) => {
        // alert("file downloaded at: " + self.file.dataDirectory);
        if (stuff != null) {
          // self.socialSharing.share('CSV file export', 'CSV export', url, '')
          self.socialSharing.share('CSV file export', 'CSV export', stuff['nativeURL'], '')
        } else return Promise.reject('write file')
      }).catch(() => {
        alert("error creating file at :" + self.file.dataDirectory);
      });
    // });
  }

  changeDate(key) {
    this.datetimeStart = undefined;
    if (key === 'today') {
      this.datetimeStart = moment({ hours: 0 }).format();
    } else if (key === 'yest') {
      this.datetimeStart = moment().subtract(1, 'days').format();
    } else if (key === 'week') {
      this.datetimeStart = moment().subtract(1, 'weeks').endOf('isoWeek').format();
    } else if (key === 'month') {
      this.datetimeStart = moment().startOf('month').format();
    }
  }

  getgeofence1() {
    this.apicallGeofenceReport.startLoading().present();
    this.apicallGeofenceReport.getallgeofenceCall(this.islogin._id)
      .subscribe(data => {
        this.apicallGeofenceReport.stopLoading();
        this.geofencelist = data;
        console.log("geofencelist=> ", this.geofencelist)
      },
        err => {
          this.apicallGeofenceReport.stopLoading();
          console.log(err)
        });
  }

  getGeofencedata(geofence) {
    console.log("selectedVehicle=> ", geofence)
    this.Ignitiondevice_id = geofence._id;
    this.getGeofenceReport();
  }

  getGeofenceReport() {
    if (this.Ignitiondevice_id == undefined) {
      this.Ignitiondevice_id = "";
    }
    let that = this;
    this.apicallGeofenceReport.startLoading().present();
    this.apicallGeofenceReport.getGeogenceReportApi(new Date(that.datetimeStart).toISOString(), new Date(that.datetimeEnd).toISOString(), this.Ignitiondevice_id, this.islogin._id)
      .subscribe(data => {
        this.apicallGeofenceReport.stopLoading();
        this.geofenceRepoert = data;
        if (this.geofenceRepoert.length > 0) {
          this.innerFunc(this.geofenceRepoert)
        } else {
          let toast = this.toastCtrl.create({
            message: 'Report(s) not fond for selected Dates/Vehicle.',
            duration: 1500,
            position: 'bottom'
          })
          toast.present();
        }
      }, error => {
        this.apicallGeofenceReport.stopLoading();
        console.log(error);
      });
  }

  innerFunc(geofenceRepoert) {
    let outerthis = this;
    outerthis.geofenceReportdata = [];
    var i = 0, howManyTimes = geofenceRepoert.length;
    function f() {
      outerthis.locationEndAddress = undefined;
      outerthis.geofenceReportdata.push({
        'device': outerthis.geofenceRepoert[i].device,
        'vehicleName': outerthis.geofenceRepoert[i].vehicleName,
        'direction': outerthis.geofenceRepoert[i].direction,
        'timestamp': outerthis.geofenceRepoert[i].timestamp,
        '_id': outerthis.geofenceRepoert[i]._id,
        'start_location': {
          'lat': outerthis.geofenceRepoert[i].lat,
          'long': outerthis.geofenceRepoert[i].long
        }
      });

      outerthis.start_address(outerthis.geofenceReportdata[i], i);
      i++;
      if (i < howManyTimes) {
        setTimeout(f, 100);
      }
    }
    f();
  }

  start_address(item, index) {
    let that = this;
    that.geofenceReportdata[index].StartLocation = "N/A";
    if (!item.start_location) {
      that.geofenceReportdata[index].StartLocation = "N/A";
    } else if (item.start_location) {
      var payload = {
        "lat": item.start_location.lat,
        "long": item.start_location.long,
        "api_id": "2"
      }
      this.apicallGeofenceReport.getAddressApi(payload)
        .subscribe((data) => {
          console.log("got address: " + data.results)
          if (data.results[2] != undefined || data.results[2] != null) {
            that.geofenceReportdata[index].StartLocation = data.results[2].formatted_address;
          } else {
            that.geofenceReportdata[index].StartLocation = 'N/A';
          }

        })
    }
  }

}
