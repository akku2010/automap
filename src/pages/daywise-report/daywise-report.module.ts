import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DaywiseReportPage } from './daywise-report';
import { SelectSearchableModule } from 'ionic-select-searchable';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    DaywiseReportPage,
  ],
  imports: [
    IonicPageModule.forChild(DaywiseReportPage),
    SelectSearchableModule,
    TranslateModule.forChild()
  ],
})
export class DaywiseReportPageModule {}
