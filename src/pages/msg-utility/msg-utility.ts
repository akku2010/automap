import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-msg-utility',
  templateUrl: 'msg-utility.html',
})
export class MsgUtilityPage {
  islogin: any;
  obj: any = [];
  username: any;
  testList: any;
  filterObj: any = [];
  isTrade: boolean = false
  isNonTrade: boolean = false
  checkAllNonTrades: boolean = false
  checkAllTrades: boolean = false

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public viewCtrl: ViewController) {
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    if (navParams.get("param") != null) {
      this.obj = this.navParams.get("param");
      this.filterObj = JSON.parse(JSON.stringify(this.obj));
    }
    if (localStorage.getItem("AlreadyDimissed") !== null) {
      localStorage.removeItem("AlreadyDimissed");
    }
    if (localStorage.getItem('AlreadyClicked') !== null) {
      localStorage.removeItem("AlreadyClicked");
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MsgUtilityPage');
  }

  selectedArray: any = [];

  checkAll() {
    debugger
    console.log("inside")
    console.log("chlist", this.selectedArray);
    //this.back();
    this.dismiss();
  }

  getItems(event) {
    console.log(event.target.value);
    // set val to the value of the searchbar
    const val = event.target.value;
    debugger
    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {

      this.filterObj = this.obj.filter((item) => {
        console.log("search result=> " + JSON.stringify(item))
        return (item.first_name.toLowerCase().indexOf(val.toLowerCase()) > -1);
        //return (item.toString().toLowerCase().indexOf(val.toString().toLowerCase()) > -1);

      })

    } else {
      this.filterObj = this.obj;
    }
    console.log("onj", this.obj);
  }


  back() {
    this.navCtrl.push("FastagListPage", {
      "selecteduser": this.selectedArray
    })
  }

  dismiss() {
    debugger
    let data = this.selectedArray;
    this.viewCtrl.dismiss(data);
  }

  selectMember(data) {
    debugger
    if (data.checked == true) {
      this.selectedArray.push({
        "phone": data.phone
      });
    } else {
      let newArray = this.selectedArray.filter(function (el) {
        return el.phone !== data.phone;
      });
      this.selectedArray = newArray;
    }
    console.log("st", this.selectedArray);
  }
}
